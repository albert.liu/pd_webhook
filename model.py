from sqlalchemy.dialects.postgresql import JSON
import datetime
# from datetime import datetime
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.sql.sqltypes import TIMESTAMP
db = SQLAlchemy()


class pd_webhook_db(db.Model):
    __tablename__ = "pd_webhook_db"
    id = db.Column(db.Integer, primary_key=True)
    trigger_event_or_recover_event = db.Column(db.String(255))
    incident_title = db.Column(db.String(255))
    service_id = db.Column(db.String(255))
    incident_id = db.Column(db.String(255))
    when_alert_trigger_or_recovered = db.Column(db.String(255))
   

    @property
    def serialize(self):
        return {
            "id": self.id,
            "trigger_event_or_recover_event": self.trigger_event_or_recover_event,
            "incident_title": self.incident_title,
            "service_id": self.service_id,
            "incident_id": self.incident_id,
            "when_alert_trigger_or_recovered": self.when_alert_trigger_or_recovered,
          
        }

