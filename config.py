import os

class Config:
    SECRET_KEY = os.urandom(16)
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://root:1qaz2wsx@35.234.33.106:3307/pd_webhook'
    # app.config['SQLALCHEMY_DATABASE_URI'] =  'mysql://root:!QAZ2wsx@localhost/login_database'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    # 'users' is a key that we need to write into the class

class ProdConfig(Config):
    DEBUG = False


class DevConfig(Config):
    DEBUG = True