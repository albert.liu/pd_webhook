import paramiko
from paramiko import SSHClient, SSHConfig


def test_packet_loss(ge_server_private_ip):
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect("10.7.6.165", "22", "root", "0okm5rdx")
    try:
        stdin, stdout, stderr = ssh.exec_command(
            f"ping -c5 {ge_server_private_ip}")
        list_output_result = stdout.readlines()
        packet_loss = list_output_result[-2]
        after_split = packet_loss.split("%")[0]
        packet_loss_rate = after_split.split(",")[-1]
        return packet_loss_rate
    except Exception as e:
        return e
