import base64
import time
import json
import requests


from model import db, pd_webhook_db
from config import DevConfig
from flask_migrate import Migrate
from flask import Flask, render_template, request, redirect, url_for, flash, session, jsonify, Blueprint
from flask_cors import CORS
import paramiko
from paramiko import SSHClient, SSHConfig


a10_pd_webhook = Blueprint(
    "a10_pd_webhook", __name__)


# ****************************************************************************************
@ a10_pd_webhook.route('/receiving_pg_api',methods=["GET","POST"])
@ a10_pd_webhook.route('/receiving_pg_api/',methods=["GET","POST"])
def receiving_pg_api():
    from datetime import datetime,timedelta
    if request.method == "POST":
        request_data=request.data    
        pd_webhook=json.loads(request_data.decode('utf-8'))
        print(f"pd_webhook={pd_webhook}")
        trigger_event_or_recover_event=pd_webhook["messages"][0]["event"]
        list_log_entries=pd_webhook["messages"][0]["log_entries"]
        if "trigger" in trigger_event_or_recover_event:
            print("trigger event")
            trigger_event_or_recover_event = "trigger"

        elif "acknowledge" in trigger_event_or_recover_event:
            print("incident acknowledge")
            trigger_event_or_recover_event = "acknowledge"
            
        elif "resolve" in trigger_event_or_recover_event:
            print("incident resolve") 
            trigger_event_or_recover_event = "resolved"
        for each_row in list_log_entries:
            incident_title=each_row["service"]["summary"]
            service_id=each_row["incident"]["id"]
            created_at=each_row["created_at"]
            alert_created_or_recovery_time=created_at.replace("T"," ").replace("Z","")
            print(f"alert_created_or_recovery_time={alert_created_or_recovery_time}")
            list_incident_id_and_shot_title=each_row["incident"]["summary"].split("]")
            incident_id=list_incident_id_and_shot_title[0].replace("[#","")
            incident_title=list_incident_id_and_shot_title[1]
            date_object_alert_created_or_recovery_time = datetime.strptime(alert_created_or_recovery_time, "%Y-%m-%d %H:%M:%S")
            print(f"date_object_alert_created_or_recovery_time={date_object_alert_created_or_recovery_time}")
            when_alert_trigger_or_recovered = date_object_alert_created_or_recovery_time+ timedelta(hours=8)


            print(f"when_alert_trigger_or_recovered={when_alert_trigger_or_recovered} / trigger_event_or_recover_event={trigger_event_or_recover_event} / incident_title={incident_title} / service_id={service_id} / incident_id={incident_id}")
            whole_row_to_insert = pd_webhook_db(when_alert_trigger_or_recovered={when_alert_trigger_or_recovered},trigger_event_or_recover_event=trigger_event_or_recover_event,incident_title=incident_title,service_id=service_id,incident_id=incident_id)
            db.session.add(whole_row_to_insert)
            db.session.commit()
    return "jsonify(pd_webhook)"