# from flask import Flask
# from config import DevConfig
import base64
import time
import json
import requests
from blueprint_files.a10_pd_webhook import a10_pd_webhook


from model import db, pd_webhook_db
from config import DevConfig
from flask_migrate import Migrate
from flask import Flask, render_template, request, redirect, url_for, flash, session, jsonify, Blueprint
from flask_cors import CORS
import paramiko
from paramiko import SSHClient, SSHConfig

#########Migrations#############
app = Flask(__name__)
CORS(app)

migrate = Migrate(app, db)
app.config.from_object(DevConfig)

db.init_app(app)


############## register the blueprint#############

app.register_blueprint(a10_pd_webhook)

# ****************************************
